package com.codeKata.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codeKata.checkout.Checkout;
import com.codeKata.checkout.PriceRules;

/**
 * Controller connects REST Api checkout logic.
 * @author Nikhil007
 *
 */
@RestController
public class CheckoutController {

	@RequestMapping(value = "/{value}", method= RequestMethod.GET)
	public int calculatePrice(@PathVariable("value") String value) {
		PriceRules rules = new PriceRules();
		Checkout checkout = new Checkout(rules);
		for (int i = 0; i < value.length(); i++) {
			checkout.scan(String.valueOf(value.charAt(i)));
		}
		return checkout.total();
	}
	
	@RequestMapping(value = "/", method= RequestMethod.GET)
	public String init() {
		return "Hello Code Kata";
	}
	

}