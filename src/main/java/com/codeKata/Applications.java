package com.codeKata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class to run the Checkout application.
 * 
 * @author Nikhil007
 *
 */
@SpringBootApplication
public class Applications {

	public static void main(String[] args) {
		SpringApplication.run(Applications.class, args);
	}

}
