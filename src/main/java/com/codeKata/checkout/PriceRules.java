package com.codeKata.checkout;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class reads the rules from file and stores in Cache.
 * 
 * @author Nikhil007
 *
 */
public class PriceRules {

	private final Map<String, ItemPrice> itemMap = new HashMap<>();

	public PriceRules() {
		super();
		readJSONFile();
	}

	private void readJSONFile() {
		JSONParser parser = new JSONParser();
		ItemPrice itemPrice;
		Object obj;
		try {
			obj = parser.parse(new FileReader("src/main/resources/com/codeKata/checkout/rules.properties"));

			JSONArray jsonArray = (JSONArray) obj;

			for (Object object : jsonArray) {
				JSONObject person = (JSONObject) object;
				String item = (String) person.get("item");
				long price = (long) person.get("price");
				JSONObject discount = (JSONObject) person.get("discount");
				itemPrice = new ItemPrice((int) price);
				if (discount != null) {
					long discountItems = (long) discount.get("quantity");
					long discountedAmount = (long) discount.get("price");
					itemPrice.setDiscountPrice((int) discountedAmount);
					itemPrice.setQuantity((int) discountItems);
					itemPrice.setDiscount(true);
				} else {
					itemPrice.setDiscount(false);
				}
				itemMap.put(item, itemPrice);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Map<String, ItemPrice> getItemMap() {
		return itemMap;
	}

}
