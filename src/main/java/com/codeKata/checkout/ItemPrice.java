package com.codeKata.checkout;

/**
 * this class contains price of an item along with discount values if it has any.
 * @author Nikhil007
 *
 */
public class ItemPrice {

	private final int price;
	private int quantity;
	private int discountPrice;
	private boolean discount = true;

	public ItemPrice(int price) {
		super();
		this.price = price;
	}

	/**
	 * getPrice.
	 * @return price.
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * getQuantity.
	 * @return quantity.
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * getDiscountPrice.
	 * @return discountPrice.
	 */
	public int getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * hasDiscount.
	 * @return discount.
	 */
	public boolean hasDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(boolean discount) {
		this.discount = discount;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param discountPrice
	 *            the discountPrice to set
	 */
	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

}
