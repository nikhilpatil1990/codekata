package com.codeKata.checkout;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Checkout class handles gets total price for all the input items.
 * @author npatil
 *
 */
public class Checkout {
	private Map<String, Integer> itemMap = new HashMap<>();
	private PriceRules rule;

	public Checkout(PriceRules rule) {
		this.rule = rule;
	}

	/**
	 * add item to the basket.
	 * @param item.
	 */
	public void scan(String item) {
		addItem(item);
	}

	/**
	 * get the total amount as per items selected.
	 * @return total.
	 */
	public int total() {
		int total = calculateTotalAmount();
		return total;
	}

	private int calculateTotalAmount() {

		int total = 0;
		if (rule.getItemMap() != null || !rule.getItemMap().isEmpty()) {
			Map<String, ItemPrice> ruleMap = rule.getItemMap();
			for (Entry<String, Integer> items : itemMap.entrySet()) {
				if (ruleMap.containsKey(items.getKey())) {
					ItemPrice price = ruleMap.get(items.getKey());
					total += AmountCalculator.amountCalcuator(price.hasDiscount(), items.getValue(),
							price.getQuantity(), price.getDiscountPrice(), price.getPrice());
				}
			}
		}
		return total;
	}

	private void addItem(String item) {
		if (itemMap.containsKey(item)) {
			int countOfItem = itemMap.get(item);
			itemMap.put(item, countOfItem + 1);
		} else {
			itemMap.put(item, 1);
		}
	}

	public void clearItemMap() {
		itemMap.clear();
	}
}
