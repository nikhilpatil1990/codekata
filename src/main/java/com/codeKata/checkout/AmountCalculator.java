package com.codeKata.checkout;

/**
 * Static class to calculate the discount amount or normal amount.
 * @author Nikhil007
 *
 */
public class AmountCalculator {

	public static int amountCalcuator(boolean isDiscount, int count, int discountCount, int discountPrice, int price) {
		int total;
		if (isDiscount && count >= discountCount) {
			int discount = count / discountCount * discountPrice;
			int regular = count % discountCount * price;
			total = (discount + regular);
		} else {
			total = count * price;
		}
		return total;
	}
}
